"
Cette classe est le moteur de jeux du ball bricker
"
Class {
	#name : #GameEngine,
	#superclass : #Object,
	#instVars : [
		'bricks',
		'balls',
		'threadWait',
		'execThread',
		'render',
		'thread',
		'ballStartPoint',
		'terrainLimit'
	],
	#category : #BlockBricker
}

{ #category : #test }
GameEngine >> addBall [
	|b|
	b := Ball new.
	b position: ballStartPoint.
	balls add: b.
	
]

{ #category : #accessing }
GameEngine >> ballStartPoint: aPoint [
	ballStartPoint:= aPoint.
	balls do: [:ball| ball position: aPoint ].
]

{ #category : #accessing }
GameEngine >> canvas: aCanvas [
	render:= aCanvas.
	
]

{ #category : #simulation }
GameEngine >> collisions [
	balls do: [:ball | 
		(((ball position x) <= 0) | ((ball position x) >= terrainLimit x)) ifTrue: [ ball invertXSpeed ].
		(((ball position y) <= 0) | ((ball position y) >= terrainLimit y)) ifTrue: [ ball invertYSpeed ].
		 ].
]

{ #category : #default }
GameEngine >> defaultBallStartPoint [
	^400@400
]

{ #category : #default }
GameEngine >> defaultThreadWait [
	^10
]

{ #category : #drawing }
GameEngine >> drawOn: aCanvas [
	render clear.
	bricks do: [:brick | brick drawOn: aCanvas ].
	balls do: [:ball | ball drawOn: aCanvas ].
	aCanvas changed.
]

{ #category : #initialization }
GameEngine >> initialize [ 
	super initialize.
	
	threadWait := self defaultThreadWait.
	
	bricks := Set new.
	balls := Set new.
	
	self ballStartPoint: self defaultBallStartPoint.
]

{ #category : #simulation }
GameEngine >> movBall [
	balls do: [:ball | ball computeOneStep ].
]

{ #category : #simulation }
GameEngine >> oneStep [
	self movBall.
	self collisions.
	self drawOn: render area.
]

{ #category : #game }
GameEngine >> pause [
	thread terminate.
]

{ #category : #accessing }
GameEngine >> renderSize: aPoint [
	self ballStartPoint: (aPoint x / 2.0)@(aPoint y).
	terrainLimit := aPoint.
]

{ #category : #simulation }
GameEngine >> startSimulation [
	thread := [ [true] whileTrue: [self oneStep. (Delay forMilliseconds: threadWait) wait.]] fork.
]
