"
Represente une balle dans le ball bricker
"
Class {
	#name : #Ball,
	#superclass : #Object,
	#instVars : [
		'position',
		'vitesse',
		'direction',
		'size'
	],
	#category : #BlockBricker
}

{ #category : #factory }
Ball class >> at: aPosition value: aValue [
	|b|
	b := Brick new.
	b position: aPosition.
	b value: aValue.
	^b
]

{ #category : #simulation }
Ball >> computeOneStep [
	self position: self position + vitesse.
]

{ #category : #drawing }
Ball >> drawOn: aCanvas [
	aCanvas fillOval: (Rectangle center: position-(size/2) extent: size) color: Color white.
]

{ #category : #initialization }
Ball >> initialize [ 
	super initialize.
	vitesse := 10@10.
	position := 0@0.
	size := 20@20.
	
]

{ #category : #accessing }
Ball >> invertXSpeed [
	vitesse:= (vitesse x * -1)@(vitesse y)
]

{ #category : #accessing }
Ball >> invertYSpeed [
	vitesse:= (vitesse x)@(vitesse y * -1)
]

{ #category : #accessing }
Ball >> position [
	 ^position
]

{ #category : #accessing }
Ball >> position: aPoint [
	 position := aPoint.
]
