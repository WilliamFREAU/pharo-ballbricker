"
Canvas pour le jeu Block Bricker
"
Class {
	#name : #GameCanvas,
	#superclass : #Morph,
	#instVars : [
		'area'
	],
	#category : #BlockBricker
}

{ #category : #drawing }
GameCanvas >> area [
	^ area ifNil: [
   area := FormCanvas extent: self extent.
   self clear.
   area]
]

{ #category : #default }
GameCanvas >> backgroundColor [ 
	^Color black
]

{ #category : #drawing }
GameCanvas >> clear [
	self area fillColor: self backgroundColor .
	self changed.
]

{ #category : #default }
GameCanvas >> defaultSize [
	^1000@800
]

{ #category : #drawing }
GameCanvas >> drawOn: aCanvas [
   aCanvas drawImage: self area form at: self position
	
]

{ #category : #drawing }
GameCanvas >> extent: aPoint [
   | newArea |
   super extent: aPoint.
   newArea := FormCanvas extent: aPoint.
   newArea 
      fillColor: self defaultColor;
      drawImage: self area form at: 0@0.
   area := newArea
]

{ #category : #initialization }
GameCanvas >> initialize [
	super initialize.
	self extent: self defaultSize.
	self changed.
	
]
