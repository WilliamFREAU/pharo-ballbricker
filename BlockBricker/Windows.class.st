"
Fenetre de jeux du Block Bricker
"
Class {
	#name : #Windows,
	#superclass : #Object,
	#instVars : [
		'drawingArea',
		'window'
	],
	#category : #BlockBricker
}

{ #category : #default }
Windows >> defaultSize [
	^1000@830
]

{ #category : #drawing }
Windows >> drawingArea [
	^ drawingArea ifNil: [ drawingArea := GameCanvas new. ].
]

{ #category : #initialization }
Windows >> initialize [
	super initialize.
	self installDrawingArea.
	self window openInWorld.
	self window extent: self defaultSize.
	self window position: 50@50.
	
	
]

{ #category : #drawing }
Windows >> installDrawingArea [
	self drawingArea position: 0@30.
	self window addMorph: self drawingArea.
]

{ #category : #drawing }
Windows >> window [
   ^ window ifNil: [
      window := StandardWindow labelled: 'Block Bricker'.
      "window announcer
         on: WindowResizing 
         do: [:ann | self drawingArea extent: ann newSize]."
      window
   ]
]
