"
Cette classe est destiner a contenir toute la partie jeu
"
Class {
	#name : #Jeu,
	#superclass : #Object,
	#instVars : [
		'windows',
		'gameEngine'
	],
	#category : #BlockBricker
}

{ #category : #initialization }
Jeu >> initialize [
	super initialize.
	
	windows := Windows new.
	
	gameEngine := GameEngine new.
	gameEngine renderSize: windows defaultSize-30.
	gameEngine canvas: windows drawingArea.
]

{ #category : #game }
Jeu >> newGame [
	gameEngine := GameEngine new.
	self startGame.
]

{ #category : #game }
Jeu >> pauseGame [
	
]

{ #category : #game }
Jeu >> startGame [
	gameEngine startGame.
]

{ #category : #test }
Jeu >> startTestGameEngine [
	gameEngine addBall.
	gameEngine startSimulation.
]

{ #category : #game }
Jeu >> stopGame [
	gameEngine pause.
]
