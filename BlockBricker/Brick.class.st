"
Contient toute la gestion des brick
"
Class {
	#name : #Brick,
	#superclass : #Object,
	#instVars : [
		'position',
		'size',
		'value'
	],
	#category : #BlockBricker
}

{ #category : #accessing }
Brick >> color [ 
	^Color blue
]

{ #category : #draw }
Brick >> drawOn: aCanvas [
	aCanvas fillRectangle: (Rectangle origin: self position corner: self position + self size) color: self color.
	aCanvas drawString: value asString at: ( self position + self size ) / 2.
]

{ #category : #initialization }
Brick >> initialize [
	super initialize.
	self position: 20@30.
	size := 40@40.
	
]

{ #category : #accessing }
Brick >> position [
	^position
]

{ #category : #accessing }
Brick >> position: aPoint [
	position := aPoint.
]

{ #category : #accessing }
Brick >> size [
	^size
]

{ #category : #accessing }
Brick >> value [
	^value
]

{ #category : #accessing }
Brick >> value: aValue [
	value := aValue 
]
